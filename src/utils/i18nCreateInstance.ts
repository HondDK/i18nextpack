import i18next, { i18n, InitOptions, TFunction } from 'i18next';

function i18nCreateInstance(options: InitOptions): [i18n, Promise<TFunction>] {
  const _i18next = i18next.createInstance();

  return [_i18next, _i18next.init(options)];
}

export function i18nCreateInstanceSync(options: InitOptions): i18n {

  const initOptions: InitOptions = { ...options, initImmediate: false }
  const [ _i18next ] = i18nCreateInstance(initOptions);

  return _i18next;
}

