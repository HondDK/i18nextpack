import { i18nCreateInstanceSync } from './utils/';
import { TRANSLATIONS } from './consts/Translations/ru/Common';

export const i18nextCommon = i18nCreateInstanceSync({
  lng: 'ru',
  contextSeparator: '#',
  interpolation: {escapeValue: false},
  resources:{
    ru: {
      translation: TRANSLATIONS
    }
  }
})