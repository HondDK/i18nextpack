import deepFreeze from 'deep-freeze';
import { NOTIFICATION_TRANSLATIONS } from './Notification';

export const TRANSLATIONS = deepFreeze({
  Notification: NOTIFICATION_TRANSLATIONS,
} as const);
