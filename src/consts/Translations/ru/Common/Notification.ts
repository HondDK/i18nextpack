import deepFreeze from 'deep-freeze';
export const NOTIFICATION_TRANSLATIONS = deepFreeze({

  DOWNLOAD: {
    title: 'Загрузка',
    message: 'Загрузка файла',
  },
  UPLOAD: {
    TITLE: 'Загрузка',
    MESSAGE: 'Загрузка файла',
  },
  ERROR: {
    TITLE: 'Ошибка',
  }
});